# Beam take home test
Beam take home test

## Setting up the data
1. start the database
`docker run -d --name postgres -e POSTGRES_PASSWORD=password -v ${PWD}/database/:/var/lib/postgresql/data -p 5432:5432 postgres`
2. migrate and seed the database (once the script finishes you might have to to `ctrl-c`)
`yarn db:setup`


## Usage
1. start the database
`docker run -d --name postgres -e POSTGRES_PASSWORD=password -v ${PWD}/database/:/var/lib/postgresql/data `
3. start the client and server
`yarn start`


## Test cases
- Set `longitude` field to `-122.4376` and `latitude` field to `37.7577`
- In each test case, adjust the `scooters` field to view different number of scooters.
- There are 12 markers in the database so setting `scooters` to >= `12` will ensure retreiving the max possible scooters for a given location.

1. set `radius` to `100`, expected maximum markers available on map: `4`
2. set `radius` to `300`, expected maximum markers available on map: `7`
3. set `radius` to `2000`, expected maximum markers available on map: `9`
4. set `radius` to `3000`, expected maximum markers available on map: `12`

## Run tests
1. Run client tests
`yarn test-client`
2. Run server tests
`yarn test-server`

## Troubleshooting
1. remove `node_modules` and run `yarn`
1. run `yarn db:reset`
2. ensure node version is `v15.11.0`

