const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const seedData = [
  { latitude: 37.78, longitude: -122.41 },
  { latitude: 37.74, longitude: -122.441139 },
  { longitude: -122.437966, latitude: 37.757793 },
  { longitude: -122.438095, latitude: 37.758168 },
  { longitude: -122.437956, latitude: 37.757169 },
  { longitude: -122.437897, latitude: 37.756567 },
  { longitude: -122.436949, latitude: 37.758007 },
  { longitude: -122.437817, latitude: 37.755939 },
  { longitude: -122.439059, latitude: 37.757451 },
  { longitude: -122.432608, latitude: 37.758861 },
  { longitude: -122.445577, latitude: 37.734405 }, //futherest
  { longitude: -122.444638, latitude: 37.734262 }, //futherest
];

async function main() {
  await prisma.scooters.deleteMany();
  seedData.forEach(async d => {
    const scooter = await prisma.scooters.create({
      data: {
        longitude: d.longitude,
        latitude: d.latitude,
      },
    });

    console.log({ scooter });
  });
}

main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
