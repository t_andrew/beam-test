var express = require('express');
const { getScooters, validateParams } = require('./api/v1/scooters');

var app = express();

app.get(
  '/api/v1/scooters',
  validateParams,
  getScooters
);

app.listen(5000, () => {
  console.log('Server running on port 5000');
});
