const { getBounds, inBound } = require('./');

describe('getBounds()', () => {
  it('returns the bound box given a radius and longitude and latitude', () => {
    const longitude = -122.4376;
    const latitude = 37.7577;
    const radius = 2000;
    const expected = [
      -122.46035011517857,
      37.73971359272551,
      -122.41484988482141,
      37.7756864072745,
    ];

    expect(getBounds(longitude, latitude, radius)).toEqual(expected);
  });
});

describe('inBound', () => {
  describe('point within bounds', () => {
    it('returns true', () => {
      const latitude = 37.7577;
      const longitude = -122.4376;

      const bbbx = [
        -122.46035011517857,
        37.73971359272551,
        -122.41484988482141,
        37.7756864072745,
      ];

      expect(inBound(longitude, latitude, bbbx)).toEqual(true);
    });
  });
  describe('point outside bounds', () => {
    it('returns false', () => {
      const latitude = 37.734405;
      const longitude = -122.445577;

      const bbbx = [
        -122.46035011517857,
        37.73971359272551,
        -122.41484988482141,
        37.7756864072745,
      ];

      expect(inBound(longitude, latitude, bbbx)).toEqual(false);
    });
  });
});
