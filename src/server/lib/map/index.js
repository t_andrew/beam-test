const {
  point,
  buffer,
  bbox,
  booleanPointInPolygon,
  bboxPolygon,
} = require('@turf/turf');

function inBound(longitude, latitude, bounds) {
  return booleanPointInPolygon(
    point([longitude, latitude]),
    bboxPolygon(bounds)
  );
}

function getBounds(longitude, latitude, radius) {
  const buff = buffer(point([longitude, latitude]), radius, {
    units: 'meters',
  });
  return bbox(buff);
}

function getPointsInBounds(longitude, latitude, radius, points) {
  const boundingBox = getBounds(longitude, latitude, radius);
  return points.filter(p => inBound(p.longitude, p.latitude, boundingBox));
}

module.exports = { getBounds, inBound, getPointsInBounds };
