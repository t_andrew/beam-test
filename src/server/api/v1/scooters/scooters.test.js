const scooterFixture = require('./fixtures/scooters.json');
const { getScooters } = require('./');
let db = require('../../../db');

const mockRequest = {
  query: { longitude: 1, latitude: 1, radius: 1, scooters: 1 },
};
const mockResponse = {
  statusCode: jest.fn().mockReturnValue(200),
  json: jest.fn().mockReturnValue(scooterFixture),
  header: jest.fn().mockReturnValue(100),
};

describe('GET /api/v1/scooters', function() {
  db = jest.fn({ fetchScooters: jest.fn() });
  it('responds with 200', async function(done) {
    await getScooters(mockRequest, mockResponse);
    expect(mockResponse.statusCode()).toEqual(200);
    done();
  });

  it('returns the payload', async function(done) {
    await getScooters(mockRequest, mockResponse);
    expect(mockResponse.json()).toEqual(scooterFixture);
    done();
  });
});
