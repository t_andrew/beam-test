const db = require('../../../db');
const { getPointsInBounds, getBounds } = require('../../../lib/map');
const { handleErrors, validateParams } = require('./helpers')
require('dotenv').config();

async function getScooters(req, res, next) {
  handleErrors(req, res);

  const { longitude, latitude, radius, scooters: numScooters } = req.query;
  const data = await db
    .fetchScooters()
    .catch(e => {
      res.status(500).json({ errors: 'Something went wrong' });
    })
    .finally(async () => {
      await db.disconnect();
    });

  const point = {
    longitude: parseFloat(longitude),
    latitude: parseFloat(latitude),
  };

  res.header('Access-Control-Allow-Origin', process.env.CLIENT_URL);
  res.json({
    data: {
      scooters: getPointsInBounds(
        point.longitude,
        point.latitude,
        radius,
        data
      ).slice(0, numScooters),
      bounds: getBounds(point.longitude, point.latitude, radius),
    },
  });
}


module.exports = { getScooters, validateParams };
