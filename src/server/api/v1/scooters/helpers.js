const { check, validationResult } = require('express-validator');

function handleErrors(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
}
const validateParams = [
  check('radius')
    .exists()
    .isInt(),
  check('scooters')
    .exists()
    .isInt(),
  check('longitude')
    .exists()
    .isFloat(),
  check('latitude')
    .exists()
    .isFloat(),
];

module.exports = { validateParams, handleErrors };
