const { PrismaClient } = require('@prisma/client');

class Db {
  constructor(client) {
    this._dbClient = client;
  }

  get client() {
    return this._dbClient;
  }

  async fetchScooters() {
    const allScooters = await this._dbClient.scooters.findMany();
    return allScooters;
  }

  async disconnect() {
    await this._dbClient.$disconnect();
  }
}

const prisma = new PrismaClient();
module.exports = new Db(prisma);
