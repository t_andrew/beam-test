const { PrismaClient } = require('@prisma/client');
const db = require('./');

describe('database client', () => {
  it('.fetchScooters queries the db for scooter data', async () => {
    db.client.scooters.findMany = jest.fn()

    const scooters = await db.fetchScooters()

    expect(db.client.scooters.findMany.mock.calls.length).toBe(1);
  })
})
