import { useState } from 'react';
import SearchBox from './components/SearchBox';
import Map from './components/Map';
import { setBounds, defaultViewportConfig } from './components/Map/helpers';
import ScooterAPI from './lib/api/scooter'
import get from 'lodash.get';

import './App.css';

function App() {
  const [viewport, setViewport] = useState({
    latitude: 37.7577,
    longitude: -122.4376,
    zoom: 10,
  });
  const [scooters, setScooters] = useState(null);
  const [radius, setRadius] = useState(null);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [markers, setMarkers] = useState([]);

  const handleOnSubmit = async e => {
    e.preventDefault();

    try {
      const payload = await ScooterAPI.getScooters(
        scooters,
        longitude,
        latitude,
        radius
      );
      setMarkers(get(payload, 'data.scooters', []));
      setViewport({
        ...viewport,
        ...setBounds(viewport, get(payload, 'data.bounds', null)),
        ...defaultViewportConfig,
      });
    } catch (err) {
      alert('Something went wrong fetching the request, please try again');
      // send error to alerting service
      console.log(err);
    }
  };

  return (
    <div className="App">
      <SearchBox
        onScooterChange={setScooters}
        onRadiusChange={setRadius}
        onLatitudeChange={setLatitude}
        onLongitudeChange={setLongitude}
        onSubmit={handleOnSubmit}
      />

      <Map points={markers} viewport={viewport} setViewport={setViewport} />
    </div>
  );
}

export default App;
