import ReactMapGL, { Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import markerImage from './marker.png';

export default function Map({ viewport, setViewport, points }) {
  const markers = points.map(s => {
    return (
      <Marker
        key={s.id}
        latitude={parseFloat(s.latitude)}
        longitude={parseFloat(s.longitude)}
        offsetLeft={-20}
        offsetTop={-10}>
        <img src={markerImage} alt="marker" />
      </Marker>
    );
  });
  return (
    <ReactMapGL
      {...viewport}
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
      width="100%"
      height="100vh"
      className="map"
      onViewportChange={viewport => setViewport(viewport)}>
      {markers}
    </ReactMapGL>
  );
}
