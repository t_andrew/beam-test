import { formatBounds } from './';

describe('.formatBounds', () => {
  it('formats parameter [] as [[],[]]', () => {
    const expected = [[1, 2], [3, 4]];

    expect(formatBounds([1, 2, 3, 4])).toEqual(expected);
  });
});
