import { WebMercatorViewport, FlyToInterpolator } from 'react-map-gl';

const setBounds = (viewport, bounds) => {
  const { longitude, latitude, zoom } = new WebMercatorViewport(
    viewport
  ).fitBounds(formatBounds(bounds), {
    padding: 0,
    offset: [0, -100],
  });

  return { longitude, latitude, zoom };
};

const formatBounds = bounds => {
  const half = Math.ceil(bounds.length / 2);

  return [bounds.splice(0, half), bounds.splice(-half)];
};

const defaultViewportConfig = {
  transitionDuration: 5000,
  transitionInterpolator: new FlyToInterpolator(),
};

export { setBounds, defaultViewportConfig, formatBounds };
