import React, { useState } from 'react';
import SearchBox from './index';
import { render, fireEvent } from '@testing-library/react';

const setup = () => {
  const props = {
    onScooterChange: jest.fn(),
    onRadiusChange: jest.fn(),
    onLatitudeChange: jest.fn(),
    onLongitudeChange: jest.fn(),
  };
  const utils = render(<SearchBox {...props} />);
  const scooterInput = utils.getByLabelText('scooters');
  const radiusInput = utils.getByLabelText('radius');
  const latitudeInput = utils.getByLabelText('latitude');
  const longitudeInput = utils.getByLabelText('longitude');
  return {
    scooterInput,
    radiusInput,
    latitudeInput,
    longitudeInput,
    ...utils,
    ...props,
  };
};

test('Scooter input should return a valid number', () => {
  const { scooterInput, onScooterChange } = setup();
  fireEvent.change(scooterInput, { target: { value: 23 } });
  expect(onScooterChange.mock.calls[0][0]).toBe(23);
});

test('Radius input should return a valid number', () => {
  const { radiusInput, onRadiusChange } = setup();
  fireEvent.change(radiusInput, { target: { value: 5 } });
  expect(onRadiusChange.mock.calls[0][0]).toBe(5);
});

test('Latitude input should return a valid number', () => {
  const { latitudeInput, onLatitudeChange } = setup();
  fireEvent.change(latitudeInput, { target: { value: 2.441822 } });
  expect(onLatitudeChange.mock.calls[0][0]).toBe(2.441822);
});

test('Longitude input should return a valid number', () => {
  const { longitudeInput, onLongitudeChange } = setup();
  fireEvent.change(longitudeInput, { target: { value: -76.606283 } });
  expect(onLongitudeChange.mock.calls[0][0]).toBe(-76.606283);
});

