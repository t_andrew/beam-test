import './SearchBox.css';

function SearchBox({
  scooters,
  radius,
  longitude,
  latitude,
  onScooterChange,
  onRadiusChange,
  onLatitudeChange,
  onLongitudeChange,
  onSubmit,
}) {
  return (
    <div className="search-box-container">
      <form className="search-box" onSubmit={onSubmit}>
        <label>
          <input
            type="number"
            placeholder="How many scooters?"
            name="scooters"
            aria-label="scooters"
            required
            value={scooters}
            onChange={({ target: { value } }) => onScooterChange(Number(value))}
          />
        </label>
        <label>
          <input
            type="number"
            placeholder="Search radius"
            name="radius"
            aria-label="radius"
            required
            value={radius}
            onChange={({ target: { value } }) => onRadiusChange(Number(value))}
          />
        </label>
        <label>
          <input
            type="number"
            placeholder="Latitude"
            name="latitude"
            aria-label="latitude"
            required
            step="0.000001"
            value={latitude}
            onChange={({ target: { value } }) =>
              onLatitudeChange(Number(value))
            }
          />
        </label>
        <label>
          <input
            type="number"
            placeholder="Longitude"
            name="longitude"
            aria-label="longitude"
            required
            step="0.000001"
            value={longitude}
            onChange={({ target: { value } }) =>
              onLongitudeChange(Number(value))
            }
          />
        </label>
        <input type="submit" value="Search" aria-label="search" />
      </form>
    </div>
  );
}

export default SearchBox;
