class ScooterAPI {
  static async getScooters(scooters, longitude, latitude, radius) {
    const searchParams = new URLSearchParams({
      scooters,
      longitude,
      latitude,
      radius,
    });

    const response = await fetch(
      `${
        process.env.REACT_APP_SERVER_URL
      }/api/v1/scooters?${searchParams.toString()}`,
      { mode: 'cors' }
    ).then(response => response.json());

    return response;
  }
}


module.exports = ScooterAPI
